[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {2,0,"V(N002,N003)/V(N004)"}
      X: ('K',1,1500,1000,4500)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: (' ',0,-200,40,200)
      Volts: ('m',0,0,0,-0.54,0.09,0.45)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 1
   }
}
[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(N002,N003)"}
      X: ('m',0,0,0.01,0.1)
      Y[0]: ('m',0,-0.54,0.09,0.45)
      Y[1]: (' ',0,1e+308,10,-1e+308)
      Volts: ('m',0,0,1,-0.54,0.09,0.45)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
